import Vue from 'vue'
import Router from 'vue-router'
import Dashboard from '@/components/dashboard'
import Sell from '@/components/sell'
import Hoz from '@/components/hoz'
import Education from '@/components/education'
import Admins from '@/components/admins'
import HR from '@/components/hr'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Dashboard',
      component: Dashboard
    },
    {
      path: '/sell',
      name: 'sell',
      component: Sell
    },
    {
      path: '/hoz',
      name: 'hoz',
      component: Hoz
    },
    {
      path: '/edu',
      name: 'education',
      component: Education
    },
    {
      path: '/admins',
      name: 'admins',
      component: Admins
    },
    {
      path: '/hr',
      name: 'hr',
      component: HR
    }
  ]
})
