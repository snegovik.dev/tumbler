import gulp            from 'gulp'
import concat          from 'gulp-concat'
import plumber         from 'gulp-plumber'
import notify          from 'gulp-notify'
import image           from 'gulp-image'
import svgSprite       from 'gulp-svg-sprite'
import spritesmith     from 'gulp.spritesmith'
import buffer          from 'vinyl-buffer'
import merge           from 'merge-stream'
import grid            from 'smart-grid'

const gridConfig = {
    outputStyle: 'styl',
    columns: 12,
    offset: "30px",
    container: {
        maxWidth: '1200px',
        fields: '30px'
    },
    breakPoints: {
        lg: {
            'width': '1100px',
            'fields': '30px'
        },
        md: {
            'width': '960px',
            'fields': '15px'
        },
        sm: {
            'width': '780px',
            'fields': '15px'
        },
        xs: {
            'width': '560px',
            'fields': '15px'
        }
    }
};

const paths = {
  image: {
    src: formatPath('src/assets/i/**/*.{jpg,png}').concat(['!src/assets/i/sprite/**/*.*']),
    dest:  'src/assets',
    watch: formatPath('src/assets/i/**/*.{jpg,png}').concat(['!src/assets/i/sprite/**/*.*'])
  },
  svg: {
    src:   formatPath('src/assets/i/sprite/svg/*.svg'),
    dest:  'src/assets',
    watch: formatPath('src/assets/i/sprite/svg/*.svg')
  },
  sprite: {
    src:   formatPath('src/assets/i/sprite/png/*.{jpg,png}'),
    dest:  'static',
    watch: formatPath('src/assets/i/sprite/png/*.{jpg,png}')
  }
}

const notifyes = {
  plumber: {
    error: notify.onError("Error: <%= error.message %>")
  }
}

export function smartGrid_init(cb){
  grid("src/helpers", gridConfig)
  cb()
}

export function image_build() {
  return gulp.src(paths.image.src)
    .pipe(plumber({errorHandler: notifyes.plumber.error}))
    .pipe(image())
    .pipe(gulp.dest(paths.image.dest))
}

export function svgSprite_build(){
  return gulp.src(paths.svg.src)
    .pipe(plumber({errorHandler: notifyes.plumber.error}))
    .pipe(svgSprite({
      svg: {
        xmlDeclaration: false,
        doctypeDeclaration: false
      },
      mode : {
        symbol : {
          dest:   '.',
          sprite: 'sprite.svg'
        }
      }
    }))
    .pipe(gulp.dest(paths.svg.dest))
}

export function pngSprite_build(){
  var spriteData = gulp.src(paths.sprite.src).pipe(spritesmith({
    imgName: 'sprite.png',
    cssName: 'sprite.styl',
    imgPath: '/static/sprite.png'
  }));

  var imgStream = spriteData.img
    .pipe(plumber({errorHandler: notifyes.plumber.error}))
    .pipe(buffer())
    .pipe(gulp.dest(paths.sprite.dest))
    .pipe(image())
    .pipe(gulp.dest(paths.sprite.dest))

  var cssStream = spriteData.css
    .pipe(gulp.dest("src/helpers"));

  return merge(imgStream, cssStream)
}

export function watch() {
  gulp.watch(paths.image.watch, image_build)
  gulp.watch(paths.svg.watch, svgSprite_build)
  gulp.watch(paths.sprite.watch, pngSprite_build)
}

const build = gulp.series(
  image_build,
  svgSprite_build,
  pngSprite_build,
  smartGrid_init,
  gulp.parallel(
    watch
  )
)
export { build }

export default build

let _cleanFileName = fileName => fileName.replace(/_(page|layout)/g,'')

function formatPath(path){
  var excludeSymbols = "[-_]"
  var result = [path]
  if(/\/\*\*/.test(path))
    result.push('!'+path.replace("/**", `/${excludeSymbols}**`))
  if(/\/\*\w*\.[*\w]/.test(path))
    result.push('!'+path.replace(/\/\*(\w*\.[*\w])/, `/${excludeSymbols}*$1`))
  return result
}
